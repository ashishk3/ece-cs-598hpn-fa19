# [The Click Modular Router](https://courses.grainger.illinois.edu/ece598hpn/fa2019/papers/click.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 
This paper describes the system design of a router built on the conventional CPUs and performing software packet processing. 
Why did it do that? The paper was written at a time when researchers' were trying to extend router's from more than routing and forwarding such 
as providing firewall, quality of service and so forth. In Software routers of the time, the hardware was proprietary and the software was linux kernel,
which was hard to modify to suit the customized hardware. So, they implement the extendable software router called **click**.

In click, the router's fundamental building block is called an **element** which consists of four attributes - input port, configuration string, output port, and 
element class. The router is then an interconnection of these elements into a directed graph. They follow the **divide and conquer** approach to 
split the router's functionality into basic elements and work on identifying and extending them.

The authors show that this approach of _modularity_ does not harm the performance of the base Linux system and also possess easy extensibility
by adding functionality such as customized scheduling, packet dropping policy, and providing differentiated services. 

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ **Evaluation on Hardware** : They are able to improve the networking performance of base Linux by using polling and efficient I/O. Even when functionality
is added, they show that Click's performance only drops gradually.

+ **Bold approach to break router design ossification** : The paper presents a way of building software routers using divide and conquer methodology
as well as bringing in more Computer Science (such as PL techniques) which is really cool.



### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ **Approach of a declarative language seems hacky** Since, much of the functionality is left to the programmer, correctness tools such as 
_type checking or verification_ should be developed for deploying in a larger network.

+ **Single Thread** : It seems features like pre-emption and blocking are not supported and so things like task(element) scheduling and yielding are 
not clear.

+ **No comparison with hardware routers**

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ **Stride Scheduling vs. Other scheduling policies** : One could explore other scheduling policies used in processor scheduling to schedule the 
elements as opposed to just executing when called.



