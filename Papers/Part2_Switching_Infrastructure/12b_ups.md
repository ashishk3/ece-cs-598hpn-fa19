# [Universal Packet Scheduling](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/ups.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 
The paper provides the case that Least Slack Time First (LSTF) algorithm is the closest to Universal Packet Scheduling in case of two or lesser congestion
points in a network. It argues that a wide range of algorithms can be implemented using LSTF.
Additionally, key network metrics such as Flow Completion Time (FCT) and Fairness can be met using LSTF scheduler.


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ The paper tries to bridge the gap between the traditional  real-time scheduling literature and network metrics such as fairness and provides a common interface
to analyse such scheduling algorithms.

+ Breaking the shackle of menu of scheduling algorithms in present switches : The paper is an effort in moving from hard-coded list of sub-optimal scheduling algorithms,
to a more generic approximately universal algorithm 


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ **Choice of Topology** The paper provides simulation results on a particular network topology of Internet-2 without saying why the particular topology was 
choosen

+ **Hardware implementation details not covered** : The paper does not go into depth of the challenges in implementing the LSTF algorithm in hardware and 
how one can use today's switches to implement the universal scheduling algorithm.
 

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ **Understanding hardware overhead** : One could analyse the hardware requirements to implement LSTF in current switches and what are the shortcomings
of current switches to implement UPS.
