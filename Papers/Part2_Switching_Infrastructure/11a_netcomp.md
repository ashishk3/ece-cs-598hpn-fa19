# [Netcache : Balancing Key-Value Stores with FAst In-Network Caching](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/netcomp.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 
In this paper, the authors analyze the trade-offs and design decisions while deploying decisions while deploying applications on programmable switches. 
The authors analyse the different hardware model (programmable hardware vs. endhost cpu vs fpga) & different
deployment model (in-fabric vs in-device) when using in-network computations. 
Additionally, the paper provides guiding principles (e.f., offload primitives as against applications) while performing in-network computation. 
Finally, the paper initiates a three-dimensional taxonomy of applications to give a sense of whether an application is suited for deploying in-network.


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ **Philosophical approach to in-network computing :** The paper attempts to give a normative lens to answer 'why' certain applications should be deployed in the network by proposing a taxonomy. The taxonomy is comprehensive and can serve as a guideline to future
 work while characterizing an application. 

+ **Open challenges well conveyed :** Challenges such as encryption, multi-tenancy, isolation are typical in any network device and they reiterate that these challenges don't go away by deploying functionality in the
 network.

### Assuming you are a reviewer for the paper, two reasons why you would reject it

1) **Few principles not elaborated :** The authors don't elaborate on fate-sharing , keeping state out of the network and minimizing interference. Their comments in this Sec 4 for points 3-5 are very generic and should be elaborated upon. 

2) **Deployment Approaches missed references :** In Section 3, the authors explain the philosophy where to do the in-network computer but don't cite which systems perform in-device and which ones perform in-fabric deployment. A table like Table 1 which proposes taxonomy would've been
 insightful here. 

3) **Design decisions vs. Evaluation :** Being a HotOS paper, the authors have used their systems discretion to evaluate various choices for placing functionality of an application. So, this is not really a disadvantage, but I'll just put it down
here.

### A follow-up idea on the paper, described very briefly in 1-2 sentences

I would like to refine Table 1 by adding the worst case time complexity numbers with omega(n) (lower bound time complexity) or theta(n) (tighter bound time complexity) 
to understand the nature of the workload better


 
 