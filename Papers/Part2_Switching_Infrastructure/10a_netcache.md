# [Netcache : Balancing Key-Value Stores with FAst In-Network Caching](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/netcache.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 
This paper is about a high performance key-value store that resides in the data plane in the network. It tell us that it is feasible to make the network do application-level 
functionality such as in-network caching for dealing with hot items. The authors provide out an implementation on P4 switch (a ToR switch connected to a bunch of storage servers).
The Netcache system improves throughput by 3-10x and reduces latency of 40% queries by 50% - making the key-value store high performant.


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ *Solves practical issue of common case* : This paper does a great job of improving the performance in the common case of high-read workloads, which is the most popular as seen from 
30:1 read:write ratio in memcached
+ *Switches are on the hot path* : It makes sense to add in-network caching logic to the switches since in case of hot queries, switches are in the line of attack. The paper provides a 
new design of the packet processing pipeline that efficiently detects (using count-min sketch), indexes (using bloom filters), stores and serves hot key-value items.
+ *Keeps up with Line Rate* : As it keeps up with line rate, it is impressive that the algorithms used are having low overhead and do not add to the complexity of the packet processing 
pipeline.
+ *Strong system performance evaluation* : The system evaluation contains a comparison of workload, different write ratios, impact of different cache sizes and storage nodes.

### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ *Variation of throughput as a function of key size* : As programmable switches evolve and we put multiple applications on it, it is key to understand the behavior under worst-case.
I would have liked to see a graph in addition to the already impressive evaluation.

+ *Evaluation not scaled out enough* : One switch + three servers is not a sufficiently realistic evaluation testbed to model evaluation in a realistic scenario.

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ **Scale across multiple racks** :As they mention in the paper, one followup is to scale it across multiple racks and employ a mechanism to minimize routing detours and maximise switch utilization.
+ **Test on 100G NICS** in addition to 40G NICs


