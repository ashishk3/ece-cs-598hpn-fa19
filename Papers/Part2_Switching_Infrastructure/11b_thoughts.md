# [Netcache : Balancing Key-Value Stores with FAst In-Network Caching](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/thoughts.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 

The paper poses the question of what functionality of the app should be deployed in the switch when deployed in a network. 
It argues that switches should only provide header parsing flexibility and more complicated logic should reside on separate network servers
or specialized hardware. The paper shows this by proposing alternate designs in two cases where in-network compute is popular - load balancing and load partitioning 
and shows how this alternate design can solve shortcomings such as state and policy. 
Lastly, the paper rounds off by highlighting areas where it makes sense to perform the work in programmable switches and areas where they believe it has been a mistake to do so.


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ **Refactoring applications to use hardware :** The paper provides a detailed analysis of state of the art in load balancing and shows how implementing everything in switch using an approach like SilkRoad can get complicated (e.g., switches have to keep track
 of back-end connections, limited switch cache for connection table). They do the same exercise for load partitioning. 

+  **Not just performance :** The authors emphasizes that measuring only performance and that too only in isolation is not the right evaluation strategy and that multiple issues 
lie beneath the surface such as state, policy flexibility and hardware limitations. The paper reiterates that programmable switches should be used when :
    * they enable new network functionality and 
    * the functionality can't be deployed end host/server.


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ **The alternate approaches proposed do not have any early results backing them.** While the alternate approaches proposed for load distribution uses systems discretion, there are no early results or simulations to back the claims. Since the paper specifically
 target SilkRoad and NetCache, this was warranted. 
+ **No generalization :** While the authors analyze several implementations in programmable switches in isolation and their detailed system analysis, a more generalization to judge whether or not and how much
 of an application should be in programmable switches is missing. Without this and without a substantial evaluation, there is not much contribution in this paper other than a few thought experiments.


### A follow-up idea on the paper, described very briefly in 1-2 sentences

The paper proposes domains where programmable switches are indispensable. 
I would like to find a common set of primitives during multi-tenancy (multiple apps deployed on the same switch) - 
The other paper says, "offload primitives, not apps" and "make primitives reusable"and I would like to use this insight to explore primitives when deploying
multiple applications such as Telemetry or Scheduling on the switch

