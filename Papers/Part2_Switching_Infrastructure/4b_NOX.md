# [NOX: Towards an Operating System for Networks](http://www.cs.yale.edu/homes/jf/nox.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 


### Paper summary 
NOX, a network operating system writen in C++, builds on the 4D idea of decision, dissemination, discovery and data planes. 
It focuses primarily on the decision/disemination plane, tries to solve the problem of the application being worried about too many low-level misconfigurations.

NOX is a system that gives the right programming abstractions in a network to enable ease of control and manageability -- events, namespaces, network views.
The paper motivates using NOX by providing an open source OS and makes a case of using it in common applications such as User-based VLAN tagging and 
anomaly detection.

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ **Example Applications are well-motivated** : The case for using NOX is strongly demonstrated by multiple example applications and the ease of manageability
of application is also demonstrated with the Ethane usecase

+ **Compatibility of NOX along with other applications** : The paper clearly states NOX deals with packets on a level of flow-granularity since
it requires maintaining network view globally that is consistent. However, at the same time it states that for applications requiring packet-level 
granularity, NOX can be integrated into a system with middleboxes and can be incrementally deployed.


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ **Responsibilities of a Network OS not clearly stated**: While the paper draws parallel to the Operating System world, it does not list out the 
specific objectives that a Network OS should achieve. The argument of providing management and control 'interfaces', is in my view slightly handwavy. In comparison,
convention OS'es have responsibilities such as process, I/O, memory management. That's why I am not convinced whether network OS'es are a need or a convenience.

+ **Library vs OS**: NOX is incorrectly called a network OS as an OS is a privileged piece of software running on every system. Instead, this seems to
be like a library that many network and management applications can use

+ **Comparison between 4D, Ethane, NOX, SANE** : Since the paper draws parallel and inspiration from these multiple papers, it is not clear to me when 
not having NOX would make manageability harder, ateast quantitatively.


### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ **Explore NOX in large-scale systems such as DPI**: A deep packet inspection system running NOX would be a good exercise to understand the 
overhead of performing per-packet management and control. Furthermore, a deployment study using NOX in a network would allow us to understand the 
long-term benefits (such as lesser misconfigurations) of using abstractions.
