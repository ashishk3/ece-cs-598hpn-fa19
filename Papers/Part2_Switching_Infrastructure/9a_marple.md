# [Language-Directed Hardware Design for Network Performance Monitoring](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/marple.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 

Marple is a performance query language for streaming queries for network statistics. It uses the idea of caching & steram processing to separate the query processing between a flow table and 
a backing store. The system does this at line rate in the data plane. Marple queries are compiled to a P4 behavioral model and a Banzai model.


However, the kind of queries that marple can support are limited by capabilities and memory in the data plane.

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ *Telemetry at Scale:* Marple can support telemetry tasks by computing aggregate statistics over a subset of trqaffic and the programming model is amenable to progammable switches
Marple can be used to diagnose very prevalent issues like microburst-based flow contention and priority-based flow contention.

+ *Replaces Adhoc methods of debugging networks* : Language-based hardware design is an innovative approach as opposed to current techniques such as switch counters, end host based 
techniques to diagnose issues.


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ *Marple cannot support join after an aggregration operator like reduce.*

+ *High resource requirement of 1 key-value server to support queries for a 64 10 GbE switch*


### A follow-up idea on the paper, described very briefly in 1-2 sentences


