# [OpenFlow: Enabling Innovation in Campus Networks](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/openflow.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 


### Paper summary 
Tagline: "Make vendors add OpenFlow support to their switches while keeping their underlying implementation proprietary".

The paper highlights the issues facing networks of the age - difficulty to control and manage, slow pace of research and experimentation. They rootcause this to the high barrier for
testing new ideas. As a result, they make an effort to use Campus networks and wiring closets to act as a testbed for new ideas while maintaining a separation between production
and experimental traffic.

Towarrds this, they propose the OpenFlow switch, and identify a common set of functions that run in a large group of routers and switches. 
Their proposed switch consists of three parts: 1) Flow Table, 2) Secure Channel and, 3) OpenFlow Protocol.

They talk about the architecture of two kinds of switches -- Dedicated OpenFlow switch and Openflow-enabled switch and how one could go about developing them. 
They highlight the details of the OpenFlow switches and the benefits of enabling OpenFlow on switches for vendors.

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ **Well-laid out Motivation:** The paper rightly captures the reason of ossification of networking research and the difficulty in experimentation of new protocols and 
also puts a case for adoption by manufactures by having an arrangement that keeps their implementation proprietary while adhering to OpenFlow protocol.

+ **Diverse nature of network clients are considered** : The paper considers scenarios like Mobile clients, VoIP clients, non-IP networks and special scenarios such as intrusion
detection systems and suggestions on how OpenFlow-enabled switches could be used in such a scenario. While some of the reasons (like controller tracking mobile client locations)
are not very convincing without data, discussion of the issues is a step in the right direction.

+ **Explicitly calls out both Controller and Switch responsiblities**  OpenFlow-enabled switches need controllers to ensure that only the right entity should be able to add the flow rules, and switches
need to have actions which will distinguish between normal and experimental packet processing pipeline.



### Assuming you are a reviewer for the paper, two reasons why you would reject it


+ **Lack of prototype** : The paper points to Ethane paper to solve the performance and port density issues with realizing such an OpenFlow-enabled switch and towards NOX
paper for a controller implementation. This barely suffices, and a discussion with performance numbers is warranted to analyse feasibility in terms of scalability and performance.

+ **IP Networks only**: The paper mentions OpenFlow Type 0 switch which has header fields matched over IP networks only and does not cover other protocols like MPLS. 


### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ **Comparison of Reference Designs for Type-0 switch** : One could exploring challenges of implementing a Type-0 switch such as Linux Boxes, NetFPGA and other reference designs in detail and comparetheir performance, security and ease of use.




