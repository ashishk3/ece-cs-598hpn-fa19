# [Towards Programmable Packet Scheduling](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/pifo.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 

This paper shows the design and feasibility of implementation of programmable scheduler algorithms. It describes how the primitives of  priority and 
departure time can be generalized using abstractions of priority and calendar queues. 

The authors claim that these abstraction can be realized using a push-in-first-out data structure and shows with simulations that this data structure can 
be realized in practice and can be used to implement all the *common* scheduling algorithms such as pFabric, Token Bucket Shaping and Start-Time Fair Queuing 
to name a few.


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ The paper describles argumentatively the **Modularity of Scheduling Algorithm implementation** using PIFO hardware block and shows that all the 
commonly used scheduling algorithms can be implemented using a PIFO block. This paper thus brings in programmability in an aspect of the switch pipeline
which is tradionally inflexible till now.

+ **Gives direction to Programmable Hardware vendors** As their hardware implementation using simulation shows the feasibility of the approach, 
this paper could be useful to hardware designers to guide future implementation of programmable switches.


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ **No quantitative argument** of universality : This paper describles different algorithms on a case by case basis and does not provide examples of cases
when PIFO would not be able to model the scheduling algorithm.

+ **Trade-offs other than chip area** not addressed. The paper touches upon memory fragmentation issues and claims that a worst-case fragmentation can be 
established. However, they don't go into details.

+ ** CAM Update Overhead** : The paper claims that when mini PIFO's get full, they are split into further two mini PIFO's which require CAM updation via multiple
writing ports. Wouldn't this blow up?

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ ** Detailed mathematical analysis of hardware overhead** : A follow-up paper on worst-case guarantees of performance under conditions of memory fragmentation,
memory overflow would be useful to understand and analyze the hardware overhead better and can make for a stronger case for implementing PIFO in hardware

