# [A Survey of Active Network Research](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/activenetworking.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 


### Paper summary 
**Tag line of the paper : "Make users program their networks"** <br>
This paper, as the title suggests is a survey paper on the domain of active networking. Active networking, as opposed to conventional store-and-forward networking is a paradigm where
the network _can perform computations on and modify packet contents_. As a result the network, is expressed as a composition of many smaller programs and thus making it possible to reason
about correctness using formal methods and developments in compilers, operating systems (OS) and programming languages(PL). The paper, proposes two approaches to achieve active networks - 1) Programmable Switch Approach, and
2) Capsule Approach.

The goal of active networks, as the paper alludes to, is to accelerate network innovation and take it from a mainframe mindset (where hardware and software innovation is coupled)
to a virtualization mindset (where hardware and software can innovate separately). 

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ _Clear Motivation with Applications_ : The motivation for the paper is well-laid out and expressed clearly with new applications such as making web caches smarter, making network simulations smarter, making network monitoring 
better (by filtering out uninteresting data) and fault detection.
+ _Makes a case by highlighting developments in other domains in CS_ The paper utilises research in PL, Compilers and OS to propose a framework consisting of 2 approaches which we could take to make networks active. 
Additionally, it translates these developments into networking metrics such as mobility, efficiency and safety and compares them.
+ _Backward Compatible_ : Interoperability of active routers with legacy routers makes active networking an asset to have as it can augment current networks with modern research more easily.
+ _Well-written_ : The survey paper also classifies the different research carries out in different universities which gives a great overview of where the research is headed towards.


### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ _No Comparison_ : The paper does not give a comparison between the two approaches to active networking and when one should choose programmable switches over capsules and vice-versa. A discussion on
that lines seem missed.
+ _Lack of detail in Programming Model_ : In the section "Towards a common programming model", the paper describes how compilers and PL can support active networking by means of program encoding but it does not go into the 
same level of detail while describing the OS support for resource allocation such as access to node storage, "path"-based scheduling, reorganizing protocol code especially in the 
context of node interoperability.(table 3 has some details but it doesn't go over the challenges)
+ _Puts burden of security on end-users_ : Active networking would make deploying a new application complex as now users not only have to generate data but also need to program the network (esp if they don't want to rely on operators)
E.g., If Mallory controls a router in the path from Alice to Bob, she can compromise the communication. Scenarios such as these could be addressed better in the paper.

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ _Add Hardware Revolution_ : Today, another revolution is that of programmable hardware (NICs, FPGAs, Accelerators). One could imagine combining hardware revolution with PL, OS and Formal methods to rethink about active networking in today's context.
+ _P4 and SDN born from this paper?_ : I think a lot of ideas from the paper such as separating injection of programs from the processing of messages (one aspect of SDN today) have become full-fledged research areas on their own right.
+ _Security Projects_ : The paper talks about operator authentication while injecting programs - one could think of different ways of using blockchain or other distributed protocols for securely verifying the operator authenticity
in a distributed setting.
+ _Internet vs. Specialized Networks_ : Due to the diverse, large-scale nature of the internet, active networking might be difficult to deploy but it might be practical for small, trusted, specialized networks such as a data-center and 
enterprise networks where one can trust the capsules (speaking intuitively here).