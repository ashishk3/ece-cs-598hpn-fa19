# [The Design Philosophy of the DARPA Internet Protocols](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/designphilosophy.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 


### Paper summary 

This paper covered the philosophy behind the protocol specifications of the internet viz., why the design of the internet protocols was done the way it was.
First, it talks about the goals of the internet realization and the reasoning behind the priorities of different goals. For example, connectivity in times of failure and 
compatibility with multiple services and multiple networks was given more priority than accounting or cost-effectiveness. It goes into each of these goals in detail and talks about 
many facets of network architecture design such as

+ Separating a specific service model (TCP) from a generic building block of the network (datagram), 
+ What were the problems plaguing networks of the day (network simulators, verification mechanisms)
+ Design issues in TCP (sequence number for packets vs. bytes, issues with EOL field, reducing retransmissions and so forth)
+ Where should state be stored in a network? Should it be at the end host or should it be the network - The paper advocates for a stateless network (aka datagram)
keeping the logic at the end hosts for reliability - known as the "fate sharing" model


### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ First, the paper fills in the gap between protocol specification and protocol implementation viz., protocol design philosophy. Additionally, the paper foresees many issues that
could happen in the future with protocol implementation (poor endhost implementation could affect the network)
+ Secondly, the paper gives arguments of why TCP should be separated from IP & why TCP alone is not enough to serve the internet. Additonally, it talk about futuristic research problems in networking such as network verification and network simulation.
+ Thirdly, the paper goes into depths for describing many facets of network architecture such as supporting multiple services, supporting multiple networks and connectivity in times of failure
and how each of these aspects impacted layering of TCP/IP, datagram model and other design decisions of the internet.

### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ *Lack of Universality or Generalization*: The paper is aimed at hostile operational environments such as Military deployments and was designed with that particular application in mind.
+ *Lack of Security or Performance Considerations*: While this is a great paper, one could argue in hindsight that, much of Internet's problems today is a result of the goals that drove the internet
design philosophy. For instance, the paper does not deal with compromised hosts (*security*) and does not go into details in issues with host-resident implementations of reliability (such as congestion collapse)
+ *Curse of backward compatibility starts here*: Additionally, the paper builds on philosophy that networks should be *backward compatible* (not designing a unified network and instead interconnecting existing networks) - while this
need for backward compatibility with existing infrastructure arose out of practical considerations (economics, administrative logistics), this philosophy eventually lead to stagnation of networking research in the future.


### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ *Flipping the goals upside down*:One could think of a complementary design philosophy when accouting and resource management would be the top goal (rather than connecting multiple networks or providing 
multiple kinds of service). In such a case, one could posit that the connectionless model of the service (datagram) might not be the most accurate one. 
The paper talks about this in the last  paragraph as well when it defines flow and maintaining "soft state" (instead of hard state since resuming seamlessly from failure is not important here)

+ *Tailor-made Networks*: Additionally, one could focus on network architecture in a scenario where there is only one kind of network (say ethernet) and one kind of service 
(say avionics networks). This can lead to special network implementations such as AFDX and Time Sensitive Networking (TSN)
