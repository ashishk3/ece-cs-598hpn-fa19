# [END-TO-END ARGUMENTS IN SYSTEM DESIGN ](https://courses.engr.illinois.edu/ece598hpn/fa2019/papers/endtoend.pdf)

Paper review by Ashish Kashinath for [ECE/CS598HPN](https://courses.engr.illinois.edu/ece598hpn/fa2019/index.html) 

### Paper summary 

This paper is an arumentative one describing a problem that arises commonly in system design - _where do we place a functionality into any system?_
The paper advocates that placing functionality in the uppermost layers such as the application layer is the way forward. 

It gives two meta-reasons for this:
+ The lower levels of the systems are rarely aware of application requirements and so the functionality needs to be duplicated anyways at the application layer even if the lower levels implement it. 
+ No one wants to pay for functionality they don't use and if we have a fat lower layer incorporating every possible functionality, it might not be economically the right choice as far as accouting is concerned. 

Additionally, the paper translates this principle to TCP/IP networks and argues keeping the network as simple as possible and pushing all the functionality to the end-hosts. Furthermore, it gives multiple examples where one can apply this argument such as reliable file delivery, guaranteeing FIFO and duplicate message suppression.

### Assuming you are a reviewer for the paper, two reasons why you would accept it.

+ The reasoning behind the end-to-end argument and its application to reliability is strongly laid out and explained well with multiple examples of systems (error checking, delivery guarantees, secure transmission of data,
duplicate message suppression and transaction management). 
+ Philosophically, the paper makes the right arguments such as simplifying operations at the lower, common layers and requiring end-hosts/applications to abosrb the complexity since it
'knows for sure' if something has gone wrong.
+ The paper also covers a scenario where one does not need a strict application of an end-to-end argument viz., applications which can tolerate failure to a _certain degree_ such as digital telephony. 

### Assuming you are a reviewer for the paper, two reasons why you would reject it

+ *Lack of Details* : The paper does not go into details about which part of the application should have the logic. For instance for delivery guarantees of messages - is it the library, is it the OS kernel
or the application binary interface(ABI) that should have ACK-tracking logic.
+ *Does not talk about performance* : The examples given are more of operational/functional correctness in nature and do not take performance into consideration. There might be scenarios where
it is better to put stable, highly optimized functionality in the middle-layers.
+ *Does not talk about security* : The paper does not talk about security in the conventional sense (strictly speaking, it talks about integrity and skips confidentiality and authenticity completely). 
For instance, putting an intrusion detection functionality at the end host might delay the time to detect network intrusions.

### A follow-up idea on the paper, described very briefly in 1-2 sentences

+ Modern developments such as resource-constrained embedded systems, need for mobility (where the last link is wireless or satellite connectivity), centralized systems (such as 
Software-defined Networks) and reconfigurable systems (datacenter systems such as VL2 where puttin a virtual layer 2 aided in VM migration) would require us to revisit the principle of end-to-end arguments since 
putting a lot of functionality into the application layer might not be the best decision here and one might need every entity to aid in executing the functinality.

+ Modern applications such as MapReduce, Redis might need us to revisit the end-to-end argument so that the application designer would not need to be burdened with the low-level intricate details. 
